﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// do not run if the game is paused
		if (Time.timeScale == 0) {
			return;
		}

		// when the button is pushed, fire a bullet
		if (Input.GetButtonDown("Fire1")) {

			BulletMove bullet = Instantiate(bulletPrefab);
			// the bullet starts at the player's position
			bullet.transform.position = transform.position;

			// create a ray towards the mouse location
			Ray ray = 
				Camera.main.ScreenPointToRay(Input.mousePosition);
			bullet.direction = ray.direction;
		}
	}

}
