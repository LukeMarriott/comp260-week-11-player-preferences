﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShellMenu : MonoBehaviour {

	public GameObject shellPanel;
	public GameObject optionsPanel;

	public Dropdown qualityDropdown;
	public Dropdown resolutionDropdown;
	public Toggle fullScreenToggle;
	public Slider volumeSlider;

	public Button applyButton;
	public Button cancelButton;


	private bool paused = false;

	void Start () {
		SetPaused(paused);
		optionsPanel.SetActive(false);

		// populate the list of video quality levels
		qualityDropdown.ClearOptions();
		List<string> names = new List<string>();
		for (int i = 0; i < QualitySettings.names.Length; i++) {
			names.Add(QualitySettings.names[i]);
		}
		qualityDropdown.AddOptions(names);

		// populate the list of available resolutions
		resolutionDropdown.ClearOptions();
		List<string> resolutions = new List<string>();
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			resolutions.Add(Screen.resolutions[i].ToString());
		}
		resolutionDropdown.AddOptions(resolutions);

		// restore the saved audio volume
		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = 
				PlayerPrefs.GetFloat("AudioVolume");
		} else {
			// first time the game is run, use the default value
			AudioListener.volume = 1;
		}


	}

	void Update () {
		if (!paused && Input.GetKeyDown(KeyCode.Escape)) {
			SetPaused(true);
		}
	}

	private void SetPaused(bool p) {
		paused = p;
		shellPanel.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
	}

	public void OnPressedPlay() {
		// resume the game
		SetPaused(false);
	}

	public void OnPressedQuit() {
		// quit the game
		Application.Quit();
	}



	public void OnPressedOptions() {
		// show the options panel & hide the shell panel
		shellPanel.SetActive(false);
		optionsPanel.SetActive(true);

		// select the current quality value
		qualityDropdown.value = QualitySettings.GetQualityLevel();

		// select the current resolution
		int currentResolution = 0;
		for (int i = 0; i < Screen.resolutions.Length; i++) {
			if (Screen.resolutions[i].width == Screen.width &&
				Screen.resolutions[i].height == Screen.height) {
				currentResolution = i;
				break;
			}
		}
		resolutionDropdown.value = currentResolution;

		// set the fullscreen toggle
		fullScreenToggle.isOn = Screen.fullScreen;

		// set the volume slider
		volumeSlider.value = AudioListener.volume;


	}
		
	public void OnPressedApply() {
		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);

		// apply the changes
		QualitySettings.SetQualityLevel(qualityDropdown.value);

		Resolution res =
			Screen.resolutions[resolutionDropdown.value];
		Screen.SetResolution(res.width, res.height, true);
		Screen.SetResolution(res.width, res.height, fullScreenToggle.isOn);

		AudioListener.volume = volumeSlider.value;

		PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);


	}

	public void OnPressedCancel() {
		// return to the shell menu
		shellPanel.SetActive(true);
		optionsPanel.SetActive(false);
	}


}
